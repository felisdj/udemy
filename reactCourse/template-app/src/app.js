import React from "react"
import ReactDOM from "react-dom"
import 'normalize.css/normalize.css'
import './styles/styles.scss'

let appRoot = document.getElementById("app")

ReactDOM.render(<h1>Expensify</h1>, appRoot)