import moment from "moment"
// Get visible expenses
// const getVisibleExpenses = (expenses, filters) => {
export default (expenses, { text, sortBy, startDate, endDate }) => {
    return expenses.filter((expense) => {
        const createdAtMoment = moment(expense.createdAt)
        const startDateMatch = startDate ? createdAtMoment.isSameOrAfter(startDate) : true
        const endDateMatch = endDate ? createdAtMoment.isSameOrBefore(endDate) : true
        const textMatch = expense.description.toLowerCase().includes(text.toLowerCase())

        return startDateMatch && endDateMatch && textMatch
    }).sort( (a, b) => {
        if(sortBy === "amount") {
            return a.amount < b.amount ? -1 : 1 // -1 means a become first, 1 -> b become first
        }
        return a.createdAt < b.createdAt ? -1 : 1
    })
}