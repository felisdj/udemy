import filtersReducer from "../../reducers/filters"
import moment from "moment"

test("should setup default filter values", () => {
    const action = {
        type: "@@INIT"
    }
    const state = filtersReducer(undefined, action)

    expect(state).toEqual({
        text: "",
        sortBy: "date",
        startDate: moment().startOf("month"),
        endDate: moment().endOf("month")
    })
})

test("should set text filter action", () => {
    const text = "random text filter"
    const action = {
        type: "SET_TEXT_FILTER",
        text
    }
    const state = filtersReducer(undefined, action)

    expect(state).toEqual({
        text,
        sortBy: "date",
        startDate: moment().startOf("month"),
        endDate: moment().endOf("month")
    })
})

test("should set sort by amount", () => {
    const action = {
        type: "SORT_BY_AMOUNT",
        sortBy: "amount"
    }

    const state = filtersReducer(undefined, action)

    expect(state.sortBy).toBe("amount")
})

test("shuold set sort by date", () => {
    const currentState = {
        text: '',
        startDate: undefined,
        endDate: undefined,
        sortBy: "amount"
    }

    const action = {
        type: "SORT_BY_DATE",
        sortBy: "date"
    }

    const state = filtersReducer(currentState, action)

    expect(state.sortBy).toBe("date")
})

test("shuold set start date", () => {
    const action = {
        type: "SET_START_DATE",
        startDate: moment(0).subtract(4, 'days')
    }

    const state = filtersReducer(undefined, action)

    expect(state.startDate).toEqual(moment(0).subtract(4, 'days'))
});

test("shuold set end date", () => {
    const action = {
        type: "SET_END_DATE",
        endDate: moment(0).add(4, 'days')
    }

    const state = filtersReducer(undefined, action)

    expect(state.endDate).toEqual(moment(0).add(4, 'days'))
});