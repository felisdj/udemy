import expensesReducer from "../../reducers/expenses"
import moment from "moment"
import expenses from "../fixtures/expenses"

test("should setup default state", () => {
    const state = expensesReducer(undefined, { type: "@@INIT" })

    expect(state).toEqual([]);
})

test("should add expense object", () => {
    const expense = {
        id: "4",
        description: "Gum",
        note: "",
        amount: 150,
        createdAt: moment(0)
    }
    const action = {
        type: "ADD_EXPENSE",
        expense
    }
    let state = expensesReducer(expenses, action)

    expect(state).toEqual([ ...expenses, expense]);
})

test("should remove expense object by id", () => {
    const action = {
        type: "REMOVE_EXPENSE",
        id: expenses[1].id
    }
    const state = expensesReducer(expenses, action)
    expect(state).toEqual([ expenses[0], expenses[2] ]);
})

test("should not remove if id not found", () => {
    const action = {
        type: "REMOVE_EXPENSE",
        id: "test_id"
    }
    const state = expensesReducer(expenses, action)
    expect(state).toEqual(expenses);
})

test("should edit expense object", () => {
    const amount = 140
    const action = {
        type: "EDIT_EXPENSE",
        id: expenses[0].id,
        updates: {
            note: "edit expense 1",
            amount
        }
    }
    const state = expensesReducer(expenses, action)
    expect(state[0]).toEqual({
        ...expenses[0],
        note: "edit expense 1",
        amount
    })
})

test("should not edit expense if expense not found", () => {
    const action = {
        type: "EDIT_EXPENSE",
        id: "test",
        updates: {
            note: "edit expense 1",
            amount: 140
        }
    }
    const state = expensesReducer(expenses, action)
    expect(state).toEqual(expenses)
})

test("should set expenses", () => {
    const action = {
        type: "SET_EXPENSES",
        expenses: [expenses[1]]
    }
    const state = expensesReducer(expenses, action);
    expect(state).toEqual([expenses[1]])
})
