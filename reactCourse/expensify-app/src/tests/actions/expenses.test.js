import configureMockStore from "redux-mock-store"
import thunk from "redux-thunk"
import database from "../../firebase/firebase"
import { addExpense, removeExpense, editExpense, startAddExpense, setExpenses, startSetExpenses, startRemoveExpense, startEditExpense } from "../../actions/expenses"
import expenses from "../fixtures/expenses"

const createMockStore = configureMockStore([thunk])

beforeEach((done) => {
    const expensesData = {}
    expenses.forEach(({ id, description, note, amount, createdAt }) => {
        expensesData[id] = { description, note, amount, createdAt }
    })
    database.ref("expenses").set(expensesData).then(() => done())
})

test("should edit expense action object", () => {
    const result = editExpense("id123", { note: "New note value" })
    expect(result).toEqual({
        type: "EDIT_EXPENSE",
        id: "id123",
        updates: {
            note: "New note value"
        }
    })
})

test("should add expense action object with provided values", () => {
    const expenseData = {
        description: "Rend",
        amount: 10900,
        createdAt: 1000,
        note: "This was last months rent"
    }

    const action = addExpense(expenses[1])

    expect(action).toEqual({
        type: "ADD_EXPENSE",
        expense: expenses[1]
    })
})

test("should add expense to database and store", (done) => {
    const store = createMockStore({})

    const expenseData = {
        description: "Mouse",
        amount: 3000,
        note: "This one is better",
        createdAt: 1000
    }

    store.dispatch(startAddExpense(expenseData)).then(() => {
        const actions = store.getActions()
        expect(actions[0]).toEqual({
            type: "ADD_EXPENSE",
            expense: {
                id: expect.any(String),
                ...expenseData
            }
        })
        database.ref(`expenses/${actions[0].expense.id}`).once("value").then((snapshot) => {
            expect(snapshot.val()).toEqual(expenseData)
            done()
        })
    })

})

test("should add expense with default to database and store", (done) => {
    const store = createMockStore({})
    const expenseDefaultData = {
        description: "",
        amount: 0,
        note: "",
        createdAt: 0
    }

    store.dispatch(startAddExpense({}))
        .then(() => {
            const actions = store.getActions()
            expect(actions[0]).toEqual({
                type: "ADD_EXPENSE",
                expense: {
                    id: expect.any(String),
                    ...expenseDefaultData
                }
            })
            return database.ref(`expenses/${actions[0].expense.id}`).once("value")
        }).then((snapshot) => {
            expect(snapshot.val()).toEqual(expenseDefaultData)
            done()
        })
})

// test("should add expense action object with default values", () => {
//     const result = addExpense()

//     expect(result).toEqual({
//         type: "ADD_EXPENSE",
//         expense: {
//             id: expect.any(String),
//             description: "", 
//             note: "", 
//             amount: 0, 
//             createdAt: 0 
//         }
//     })
// })

test("should setup set expenses action object with data", () => {
    const action = setExpenses(expenses)

    expect(action).toEqual({
        type: "SET_EXPENSES",
        expenses
    })
})

test("should fetch the expenses from firebase", (done) => {
    const store = createMockStore({})
    store.dispatch(startSetExpenses()).then(() => {
        const actions = store.getActions()
        expect(actions[0]).toEqual({
            type: "SET_EXPENSES",
            expenses
        })
        done()
    })
})

test("should setup remove expense action object", () => {
    const result = removeExpense({ id: "1234" })
    expect(result).toEqual({
        id: "1234",
        type: "REMOVE_EXPENSE"
    })
})

test("should remove expense object from firebase", (done) => {
    const store = createMockStore({})
    const id = expenses[0].id
    store.dispatch(startRemoveExpense({ id })).then(() => {
        const actions = store.getActions()
        expect(actions[0]).toEqual({
            type: "REMOVE_EXPENSE",
            id
        })
        return database.ref(`expenses/${id}`).once("value")
    }).then((snapshot) => {
        expect(snapshot.val()).toBeFalsy()
        done()
    })
})

test("should edit expense action object from firebase", () => {
    const store = createMockStore({})
    const { id, description, note, amount, createdAt } = expenses[1]
    const updates = { note: "edit note value" }
    store.dispatch(startEditExpense( id, updates)).then(() => {
        const actions = store.getActions()
        expect(actions[0]).toEqual({
            type: "EDIT_EXPENSE",
            id,
            updates
        })
        return database.ref(`expenses/${id}`).once("value")
    }).then((snapshot) => {
        expect(snapshot.val()).toEqual({
            description,
            amount,
            createdAt,
            note,
            ...updates
        })
        expect(snapshot.val().note).toBe(updates.note)
        done()
    })
})