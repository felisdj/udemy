import configureMockStore from "redux-mock-store"
import thunk from "redux-thunk"
import { login, startLogin, logout, startLogout } from "../../actions/auth"

const createMockStore = configureMockStore([thunk])

test("should generate login action object", () => {
    const result = login("anything")
    expect(result).toEqual({
        type: "LOGIN",
        uid: "anything"
    })
})

test("should generate logout action object", () => {
    const result = logout()
    expect(result).toEqual({
        type: "LOGOUT"
    })
})

