import React from "react"
import ReactShallowRenderer from "react-test-renderer/shallow";
import Header from "../../components/Header"

import { shallow } from "enzyme"
import toJson from "enzyme-to-json"

test("should render Header corrlectly", () => {
    // const renderer = new ReactShallowRenderer()
    // renderer.render(<Header />)
    // expect(renderer.getRenderOutput()).toMatchSnapshot();
    const wrapper = shallow(<Header />)
    expect(toJson(wrapper)).toMatchSnapshot()
})

// react-test-tenderer
