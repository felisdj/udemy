const add = (a,b) => a + b
const generateGreeting = (name = "Anonymus") => `Hello ${name}!`

test("should add two numbers", () => {
    const result = add(1,3)

    expect(result).toBe(4)
})

test("should generate greeting from name", () => {
    const result = generateGreeting("Felis")
    expect(result).toBe("Hello Felis!")
})

test("should generate greeting for no name", () => {
    const result = generateGreeting()
    expect(result).toBe("Hello Anonymus!")
})