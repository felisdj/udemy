import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth'
import moment from "moment"

const config = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIREBASE_DATABASE_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID
};
firebase.initializeApp(config);

const database = firebase.database()
const googleAuthProvider = new firebase.auth.GoogleAuthProvider()

export { firebase, googleAuthProvider, database as default } 

/*
// child_remove
const onChildRemoved = database.ref("expenses").on("child_removed", (snapshot) => {
    console.log("child removed :", snapshot.key, snapshot.val())
})
// child_changed
database.ref("expenses").on("child_changed", (snapshot) => {
    console.log("child changed :", snapshot.key, snapshot.val())
})
// child_moved
database.ref("expenses").on("child_moved", (snapshot) => {
    console.log("child_moved :", snapshot.key, snapshot.val())
})
// child_added
database.ref("expenses").on("child_added", (snapshot) => {
    console.log("child_added :", snapshot.key, snapshot.val())
})

// challenge expenses
// const expenses = [{
//     description: "Rent",
//     note: "Mar, 2019",
//     amount: 6000,
//     createdAt: moment().valueOf()
// }, {
//     description: "CitiCredit",
//     note: "",
//     amount: 5000.17,
//     createdAt: moment().valueOf()
// }, {
//     description: "Water",
//     note: "",
//     amount: 100,
//     createdAt: moment().valueOf()
// }, {
//     description: "Home",
//     note: "",
//     amount: 3000,
//     createdAt: moment().valueOf()
// }]

// expenses.forEach((expense) => {
//     database.ref("expenses").push(expense)
// })

// database.ref("expenses").once("value").then((snapshot) => {
//     const expenses = []
//     snapshot.forEach((childSanpshot) => {
//         expenses.push({
//             id: childSanpshot.key,
//             ...childSanpshot.val()
//         })
//     })
//     console.log("expenses :", expenses)
// })

// database.ref("expenses")
//     .on("value", (snapshot) => {
//         const expenses = []
//         snapshot.forEach((childSanpshot) => {
//             expenses.push({
//                 id: childSanpshot.key,
//                 ...childSanpshot.val()
//             })
//         })
//         console.log("expenses :", expenses)
//     }, (e) => {

//     })


// array on firebase database
// const notes = [{
//     id: "12",
//     title: "First Note!!",
//     body: "this is note"
// }, {
//     id: "13",
//     title: "Second Note!!",
//     body: "this is my note"
// }]

// const notes = {
//     0 : {
//         id: "2",
//         title: "First Note!!",
//         body: "this is note"
//     }, 
//     2 : {
//         id: "3",
//         title: "Second Note!!",
//         body: "this is my note"
//     }
// }

// database.ref("notes").set(notes).then(() => {
//     console.log("set note completed.")
// }).catch((e) => {
//     console.log("set note error :", e)
// })

// push is random child node id
// database.ref("notes").push({
//     title: "test push",
//     body: "this is body"
// })

database.ref("notes").once("value").then((snapshot) => {
    console.log(snapshot.val())
}).catch((e) => {
    console.log(e)
})

/*
// set, update, remove data

// database.ref().set({
//     name: "Felis",
//     age: 26,
//     isSingle: true,
//     location: {
//         city: "Bangkok",
//         country: "Thailand"
//     },
//     updatedAt: moment().valueOf()
// }).then(() => {
//     console.log("Data is saved.")
// }).catch((error) => {
//     console.log("error log :", error)
// })

// database.ref("isSingle")
//     .remove()
//     .then(() => {
//         console.log("remove completed.")
//     }).catch((err) => {
//         console.log("did not remove data", err)
//     })

// database.ref().update({
//     name: "Felis Domesticus",
//     age: 23,
//     job: "Software developer",
//     "location/city": "new City", // <-- update single key in object data.
//     isSingle: null
// }).then(() => {
//     console.log("updated.")
// }).catch((e) => {
//     console.log(e)
// })

// fetch data
// database.ref().once("value").then((snapshot) => {
//     console.log(snapshot.val())
// }).catch((e) => {
//     console.log("fetching data", e)
// })

// fetch realtime
const onValueChange = (snapshot) => {
    const { name, age, location } = snapshot.val()
    console.log(`${name} live in ${location.city} and he's ${age} year(s) old.`)
}
const onError = (e) => {
    console.log("realtime error :", e)
}

database.ref().on("value", onValueChange, onError)
// database.ref().on("value", (snapshot) => {
//     console.log("On value :", snapshot.val())
// })

setTimeout(() => {
    database.ref().update({
        age: 24,
        "location/city": "Bangkok"
    }).then(() => {
        console.log("update completed.")
    }).catch((e) => {
        console.log("update error :", e)
    })
}, 2000);

setTimeout(() => {
    // database.ref().off() // <-- unsubscription all
    database.ref().off("value", onValueChange) // <-- unsubsription onValue function
}, 4000);

setTimeout(() => {
    database.ref().update({
        age: 25
    })
}, 6000); // <-- this's not log on console


*/