import React from "react"
import { connect } from "react-redux"

import ExpenseListItem from "./ExpenseListItem"
import selectExpenses from "../selectors/expenses"

const ExpenseList = (props) => (
    <div className="content-container">
        <div className="list-header">
            <div className="show-on-mobile">Expense List</div>
            <div className="show-on-desktop">Expense</div>
            <div className="show-on-desktop">Amount</div>
        </div>
        <div className="list-body">
            {
                props.expenses.length == 0 ? (
                    <div className="list-item--message">
                        <span>No Expenses</span>
                    </div>
                ) : (
                    props.expenses.map((expense) => 
                        <ExpenseListItem 
                            edit={props.onClick} 
                            key={expense.id} 
                            {...expense} 
                        />
                    )
                )
            }
            {/* {props.filters.text}
            {props.expenses.length} */}
        </div>
    </div>
)

const mapStateToProps = (state) => {
    return {
        expenses: selectExpenses(state.expenses, state.filters)
        // expenses: state.expenses,
        // filters: state.filters
    }
}

const ConnectedExpenseList = connect(mapStateToProps)(ExpenseList)

export default ConnectedExpenseList