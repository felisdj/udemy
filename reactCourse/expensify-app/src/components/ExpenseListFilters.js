import React from "react"
import { connect } from "react-redux"
import moment from "moment"
import { DateRangePicker } from "react-dates"
import { setTextFilter, sortByDate, sortByAmount, setStartDate, setEndDate } from "../actions/filters"
import "react-dates/initialize"
import "react-dates/lib/css/_datepicker.css"

class ExpenseListFilters extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            calendarFocused: null,
            error: ""
        }
        this.onDatesChange = this.onDatesChange.bind(this)
        this.onFocusChange = this.onFocusChange.bind(this)
    }

    onDatesChange({ startDate, endDate }) {
        console.log(moment(startDate), endDate)
        this.props.dispatch(setStartDate(startDate))
        this.props.dispatch(setEndDate(endDate))
    }
    onFocusChange( calendarFocused ) {
        this.setState( () => ({ calendarFocused }) )
    }

    render() {
        return (
            <div className="content-container">
                <div className="input-group">
                    <div className="input-group__item">
                        <input 
                            className="text-input"
                            type="text" 
                            placeholder="Search expenses"
                            value={this.props.filters.text} 
                            onChange={(e) => {
                                this.props.dispatch( setTextFilter(e.target.value) )
                            }}
                        />
                    </div>
                    <div className="input-group__item">
                        <select 
                            className="select"
                            value={this.props.filters.sortBy}
                            onChange={(e) => {
                            const sort = e.target.value
                            if (sort === "date") {
                                this.props.dispatch( sortByDate() )
                            } else {
                                this.props.dispatch( sortByAmount() )
                            } 
                        }}>
                            <option value="date">Date</option>
                            <option value="amount">Amount</option>
                        </select>
                    </div>
                    <div className="input-group__item">
                        <DateRangePicker
                            startDate={this.props.filters.startDate}
                            startDateId="your_unique_start_date_id"
                            endDate={this.props.filters.endDate}
                            endDateId="you_unique_end_date_id"
                            onDatesChange={this.onDatesChange}
                            focusedInput={this.state.calendarFocused}
                            onFocusChange={this.onFocusChange}
                            showClearDates={true}
                            numberOfMonths={1}
                            isOutsideRange={() => false}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        filters: state.filters
    }
}

export default connect(mapStateToProps)(ExpenseListFilters)