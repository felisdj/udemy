import React from "react"
import Lottie from "react-lottie"
import * as animationData from "./sandglass-loader.json"

const LoadingPage = () => {
    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: "xMidYMid slice"
        }
    }
    return (
        <div className="loader">
            <Lottie 
                options={defaultOptions}
                height={120}
                width={120}
                isClickToPauseDisabled={true}
            />
        </div>
    )
}

export default LoadingPage