import React from "react"
import ExpenseList from "./ExpenseList"
import ExpenseListFilters from "./ExpenseListFilters"
import ExpensesSummary from "./ExpensesSummary"

const Dashboard = (props) => (
    <div>
        {/* This is from my dashboard component */}
        <ExpensesSummary />
        <ExpenseListFilters />
        <ExpenseList 
            onClick={(id) => {
                props.history.push(`/edit/${id}`)
            }}
        />
    </div>
)

export default Dashboard