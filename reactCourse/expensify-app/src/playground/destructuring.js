//
// Object destructuring
//

console.log("destructuring.")

const person = {
    name: "Felis",
    age: 24,
    location: {
        city: "Bangkok",
        temp: 30
    }
}

const { name, age, location } = person
// const name = person.name
// const age = person.age

console.log(`${name} is ${age}.`)

const { city, temp: temperature, x = 100, y = "20"} = location

console.log(`It's ${temperature} in ${city} , (${x}, ${y})`)

const book = {
    title: "Ego is the Enemy",
    author: "Ryan Holiday",
    publisher: {
        name: 'Penquin'
    }
}

const { title: titleBook = "Anonymous", author, publisher } = book
const { name: publisherName = "Self-published" } = book.publisher
console.log(publisherName)

//
// Array destructuring
//

const address = ["1299 S Juniper Street", "Philadelphia", "Pennsylvania", "19147"]

const [street, , state] = address
console.log(`${state}`)

const item = ["Coffee (hot)", "$2.00", "$2.50", "$2.75"]
const [ itemName = "undefined", , mediumPrice, ] = item

console.log(`A medium ${itemName} costs ${mediumPrice}`)