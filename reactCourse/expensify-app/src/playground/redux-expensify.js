console.log("redux-expensify")

import { createStore, combineReducers } from "redux"

// Store creation
const store = createStore(
    combineReducers({
        expenses: expensesReducer,
        filters: filtersReducer
    })
)

store.subscribe(() => {
    const state = store.getState()
    const visibleExpenses = getVisibleExpenses(state.expenses, state.filters)
    console.log(visibleExpenses)
})

const expenseOne = store.dispatch( addExpense({ description: "Rent", amount: 100, createdAt: 10 }) )
const expenseTwo = store.dispatch( addExpense({ description: "Coffee", amount: 20, createdAt: 20 }) )

console.log(expenseOne)
// store.dispatch( removeExpense({id : expenseOne.expense.id}))
store.dispatch(
    editExpense( 
        expenseTwo.expense.id, 
        {
            amount: 30,
            note: "edit description"
        }
    )
)

store.dispatch(setTextFilter("co"))
store.dispatch(setTextFilter())
store.dispatch(sortByAmount())
store.dispatch(sortByDate())

store.dispatch(setStartDate(-200))
store.dispatch(setEndDate(200))
store.dispatch(setStartDate())
store.dispatch(setEndDate())

const demoState = {
    expenses : [{
        id: "djfsldfsafsdei",
        description: "January Rent",
        note: "This was the final payment for that address",
        amount: 6000,
        createdAt: 0
    }],
    filters: {
        text: 'rent',
        sortBy: "amount", // date or amount
        startDate: undefined,
        endDate: undefined
    }
}

const user = {
    name: "a",
    age: 24
}

const newUser = {
    ...user,
    location: "thai",
    age: 28
}
// console.log(newUser)

// Deep copies opject
const copyUser = JSON.parse(JSON.stringify(user))
// console.log(copyUser)
