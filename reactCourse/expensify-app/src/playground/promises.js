const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        // resolve("This is my resolve data.")
        reject("something error.")
    }, 1500)
})

console.log("Before")
promise.then((data) => {
    console.log("1", data)
}).catch((err) => {
    console.log("1", err)
})
promise.then((data) => {
    console.log("2", data)
}, (err) => {
    console.log("2", err)
})

console.log("After")