import { createStore } from "redux"

console.log("test redux file");

// const add = (data) => ( data.a + data.b )
const add = ({a, b}, c = 5) => ( a + b + c )

console.log(add({a: 10, b: 2}))

// Action generators - functions that return action objects
const incrementCount = (incrementBy) => ({
    type: "INCREMENT",
    incrementBy: incrementBy || 1
})
// const decrementCount = (payload = {}) => ({
const decrementCount = ({decrementBy = 1} = {}) => ({
    type: "DECREMENT",
    // decrementBy: typeof payload.decrementBy === "number" ? payload.decrementBy : 1
    decrementBy: typeof decrementBy === "number" ? decrementBy : 1
})
const setCount = ({ count = 0 } = {} ) => ({
    type: "SET",
    count
})
const resetCount = () => ({ type: "RESET" })

// Reducers
const countReducer = (state = { count : 0 }, action) => {
    console.log("Running", action.type)
    switch (action.type) {
        case "INCREMENT" :
            const incrementBy = typeof action.incrementBy === "number" ? action.incrementBy : 1
            return { count: state.count + incrementBy }
        case "DECREMENT" :
            return { count: state.count - action.decrementBy }
        case "SET" :
            return { count: action.count }    
        case "RESET" :
            return { count: 0 }
        default:
            return state
    }
}
const store = createStore(countReducer)

const unsubscribe = store.subscribe( () => {
    console.log(store.getState())
})

store.dispatch({
    type: "INCREMENT",
    incrementBy: 3
})

store.dispatch(incrementCount(2))

store.dispatch({
    type: "INCREMENT"
})

store.dispatch( resetCount() )

store.dispatch( decrementCount({ decrementBy: 6 }) )

store.dispatch( decrementCount() )

store.dispatch( setCount( {count: 100}) )

unsubscribe()
