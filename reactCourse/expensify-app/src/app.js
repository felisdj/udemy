import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import 'normalize.css/normalize.css'
import './styles/styles.scss'

import AppRouter, { history } from "./routers/AppRouter"
import configureStore from "./store/configureStore"
import { addExpense, removeExpense, editExpense, startSetExpenses } from "./actions/expenses"
import { login, logout } from "./actions/auth"
import getVisibleExpenses from "./selectors/expenses"
import LoadingPage from "./components/LoadingPage"

import { firebase } from './firebase/firebase'

const store = configureStore()

store.subscribe(() => {
    const state = store.getState()
    const visibleExpenses = getVisibleExpenses(state.expenses, state.filters)
    console.log(visibleExpenses)
})

// const expenseOne = store.dispatch( addExpense({ description: "Water bill", amount: 100, createdAt: 10 }) )
// const expenseTwo = store.dispatch( addExpense({ description: "Gas bill", amount: 20, createdAt: 20 }) )

// store.dispatch( addExpense({ description: "Rent", amount: 6000, createdAt: 0 }) )
// store.dispatch( addExpense({ description: "Dinner", amount: 40 }) )

let appRoot = document.getElementById("app")

const jsx = (
    <Provider store={store}>
        <AppRouter />
    </Provider>
)

let hasRendered = false
const renderApp = () => {
    if (!hasRendered) {
        ReactDOM.render(jsx, appRoot)
        hasRendered = true
    }
}

ReactDOM.render(<LoadingPage />, appRoot)

firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        store.dispatch(login(user.uid))
        store.dispatch(startSetExpenses()).then( () => {
            renderApp()
            if (history.location.pathname === "/") {
                history.push("/dashboard")
            }
        })
    } else {
        store.dispatch(logout())
        renderApp()
        history.push("/")
    }
})