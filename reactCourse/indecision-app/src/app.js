import React from "react"
import ReactDOM from "react-dom"
import 'normalize.css/normalize.css'
import './styles/styles.scss'

import IndecisionApp from "./components/IndecisionApp"


console.log("app.js is running!");

const obj = {
    name: "Felis",
    getName() {
        return this.name
    }
}
const getName = obj.getName.bind( { name: "test" } )
console.log(getName())

// Grab the add function from the add.js file in the utils folder
// Grab React from the react npm module
// add(2, 4)

// stateless functional component
// If that class dont use state
// const Name = () => { return (JSX) } is better to used.

// JSX - JavaScript XML

const appInfo = {
    title: 'Indecision App',
    subtitle: 'This is JSX from app.js!',
    options: []
}

const appRoot = document.getElementById("app");

const renderFormApp = () => {
    const template = (
        <div>
            <h1>{appInfo.title}</h1> 
            {appInfo.subtitle && <p>{appInfo.subtitle}</p>}
        </div>
    );

    ReactDOM.render(template, appRoot)
}

// renderFormApp()

// const User = (props) => {
//     return (
//         <div>
//             <p>Name : {props.name}</p>
//             <p>Age : {props.age}</p>
//         </div>
//     )
// }

ReactDOM.render(<IndecisionApp options={['Hello', 'World']}/>, appRoot)