// import './utils.js'
// import subtract, { square, add } from "./utils.js" 
import somethingIWant, { square, add } from "./utils.js" // <-- can rename export default
import isSenior, { isAdult, canDrink } from "./person.js"

console.log('app.js is running!')

console.log(square(4))
console.log(add(2, 5))
console.log(somethingIWant(5, 2))

console.log(isAdult(20))
console.log(canDrink(18))
console.log(isSenior(64))

// install -> import -> use
// import validator from "validator"
// console.log("Email: ", validator.isEmail('test@gsfa.me'))

// import React from "react"
// import ReactDOM from "react-dom"
let React = require('react')
let ReactDOM = require('react-dom')

let template = <p>THIS IS JSX FROM WEBPACK ทดสอบภาษาไทย</p>//React.createElement('p', {}, "test")

ReactDOM.render(template, document.getElementById("app"))