// let visibility = false
// let detail = (
//     <div>
//         <p>Hey. These are some details</p>
//     </div>
// )

// const showDetail = () => {
//     visibility = !visibility
//     render()
// }

// const appRoot = document.getElementById("app")

// const render = () => {
//     const jsx = (
//         <div>
//             <h1>Visibility Toggle</h1>
//             <button onClick={showDetail}>{visibility ? "Hide" : "Show"} details</button>
//             {
//                 visibility && detail
//             }
//         </div>
//     )

//     ReactDOM.render(jsx, appRoot)
// }

// render()

class VisibilityToggle extends React.Component {
    constructor(props) {
        super(props)
        this.handleToggleVisibility = this.handleToggleVisibility.bind(this)
        this.state = {
            visibility: false
        }
    }

    handleToggleVisibility() {
        this.setState( (prevState) => {
            return {
                visibility: !prevState.visibility
            }
        })
    }

    render() {
        return (
            <div>
                <h1>Visibility Toggle</h1>
                <button onClick={this.handleToggleVisibility}>{this.state.visibility ? "Hide" : "Show"} details</button>
                {this.state.visibility && (
                    <div>
                        <p>Hey. These are some details you can now see!</p>
                    </div>
                )}
            </div>
        )
    }
}

ReactDOM.render(<VisibilityToggle />, document.getElementById("app"))