console.log("app.js is running!");

class Person {
    constructor(name = 'Anonymous', age = 0) {
        this.name = name// || "test"
        this.age = age
//        console.log(this.name)
    }
    getDescription() {
        return `${this.name} is ${this.age} year(s) old.`
    }
    getGretting() {
//      "Hi. I'm " + this.name + "!"
        return `Hi. I'm ${this.name}!. I'm ${this.age} year${this.age > 1 ? 's' : ''} old.` // back-tick
        // return <p>Hi. I'm {this.name}!</p>
    }
}

class Student extends Person {
    constructor(name, age, major = "Undefinded") {
        super(name, age)
        this.major = major
    }
    getDescription() {
        return `${super.getDescription()} ${this.hasMajor() ? `My major is ${this.major}` : "I'm not have major."}.`
    }
    hasMajor() {
        /*
        parse String to Bool
        !String --> Bool
        !''     --> true
        !!''    --> false
        !'test' --> false
        */
        return !!this.major
    }
}

class Traveler extends Person {
    constructor(name, age, homeLocation) {
        console.log(arguments)
        super(name, age)
        this.homeLocation = homeLocation
    }
    getGretting() {
        return super.getGretting() + `${this.homeLocation ? ` I'm visiting from ${this.homeLocation}` : ''}`
    }
}

class Pet {
    constructor(props) {
        this.name = props.name || "Anonymous"
        this.owner = props.owner || new Person()
    }
    getDescription() {
        return `${this.name}'s owner is ${this.owner.name}`
    }
}


// JSX - JavaScript XML

const me = new Student("Felis", 23, "Computer Engineering")
console.log(me.getDescription())
console.log(me.getGretting())

const myCat = new Pet({ name: "Jake", 
                        owner: me })
console.log(myCat.getDescription())

const other = new Person()
console.log(other.getGretting())

const traveler = new Traveler("Jack", 25, "Bangkok")
console.log(traveler.getGretting())

const appInfo = {
    title: 'Indecision App',
    subtitle: 'This is JSX from app.js!',
    options: []
}

const appRoot = document.getElementById("app");

const renderApp = () => {
    const template = (
        <div>
            <h1>{appInfo.title}</h1> 
            {appInfo.subtitle && <p>{appInfo.subtitle}</p>}
            {/* {me.getGretting()} */}
        </div>
    );

    ReactDOM.render(template, appRoot)
}

renderApp()

