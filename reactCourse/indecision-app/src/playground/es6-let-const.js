let nameVar = "Felis"
nameVar = "Another"

const constantVar = "test"

console.log('nameVar :', nameVar)


function getPetName() {
    let petName = "Hal"
    return petName
}

const petName = getPetName()
console.log(petName)

// Block scoping
const fullName = "Felis J";
let lastName;
// var firstName
if (fullName) {
    var firstName = fullName.split(' ')[0]
    lastName = fullName.split(' ')[1]
    console.log(firstName)
}

console.log(firstName)
console.log(lastName)

// arrowFunction

const square = function (x) {
    return x * x
}
/* 
function square(x) {
    return x * x
}
*/

// const squareArrow = (x) => {
//     return x * x
// }


const squareArrow = (x) => x * x

console.log(squareArrow(6))

// Challenge - Use arrow functions
function getFirst(name) {
    return name.split(' ')[0]
}
const getFirstName = (name) => name.split(' ')[0]
const name = "Mike Smith"
console.log(getFirstName(name), getFirst(name))

// arguments object - no longer bound with arrow functions

const add = (a, b) => {
    // console.log(arguments)
    return a + b
}

console.log(add(55, 1, 1001))

// this keyword - no longer bound

const user = {
    name: 'Felis',
    cities: ['Songkhla', 'Bangkok'],
    printPlacesLived() {
        // console.log(this.name)
        // console.log(this.cities)
        const that = this
        
        this.cities.forEach(function (city) {
            console.log(that.name + ' has lived in ' + city);
            
        })
        this.cities.forEach( city => {
            console.log(this.name + ' in ' + city)
        })
        return this.cities.map( city => this.name + ' has lived in ' + city )
    }
}
// arrow function scope on super

console.log(user.printPlacesLived())

// Challenge area

const multiplier = {
    // numbers - array of numbers
    // multiplyBy - single number
    // multiply - return a new array where the number have been multiplired
    numbers: [4, 5, 99],
    multiplyBy: 2,
    multiply: function() {
        return this.numbers.map( (number) => number * this.multiplyBy )
    }
}

console.log(multiplier.multiply())

