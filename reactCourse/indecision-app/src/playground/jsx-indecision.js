console.log("app.js is running!");

const obj = {
    name: "Felis",
    getName() {
        return this.name
    }
}
const getName = obj.getName.bind( { name: "test" } )
console.log(getName())

class IndecisionApp extends React.Component {
    render() {

        const title = "Indecision"
        const subTitle = "Put your life in the hands of a computer"
        const options = ["Thing one", "Thing two", "Thing four"]

        return (
            <div>
                <Header title={title} subtitle={subTitle} />
                <Action />
                <Options options={options} />
                <AddOption />
            </div>
        )
    }
}

class Header extends React.Component {
    render() {
        return (
            <div>
                <h1>{this.props.title}</h1>
                <h2>{this.props.subtitle}</h2>
            </div>
        )
    }
}

class Action extends React.Component {
    handlePick() {
        alert("handlePick")
    }

    render() {
        return (
            <div>
                <button onClick={this.handlePick}>What should I do?</button>
            </div>
        )
    }
}

// class Remove extends React.Component {
//     handleRemoveAll() {
//         alert("remove all option")
//     }

//     render() {
//         return (
//             <div>
//                 <button onClick={this.handleRemoveAll}>Reset all</button>
//             </div>
//         )
//     }
// }

class Options extends React.Component {
    constructor(props) {
        super(props)
        this.handleRemoveAll = this.handleRemoveAll.bind(this)
    }

    handleRemoveAll() {
        console.log(this.props.options)
    }

    render() {
        return (
            <div>
                {/* <Remove /> */}
                <button onClick={this.handleRemoveAll}>Remove all</button>
                {
                    this.props.options.map( (option, index) => <Option key={index} optionText={option} /> )
                }
            </div>
        )
    }
}

class Option extends React.Component {
    render() {
        return (
            <div>
                Option : {this.props.optionText}
            </div>
        )
    }
}

class AddOption extends React.Component {
    handleAddOption(e) {
        e.preventDefault();
        
        const option = e.target.elements.optionField.value.trim() // --> e.(event target).(All Elements).(element's name).value
        
        if (option) {
            appInfo.options.push(option)
            console.log('option :', appInfo.options)
            alert(option)
        }
        e.target.elements.optionField.value = '';

    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleAddOption}>
                    <input type="text" name="optionField"></input>
                    <button>Add option</button>
                </form>
            </div>
        )
    }
}

// JSX - JavaScript XML

const appInfo = {
    title: 'Indecision App',
    subtitle: 'This is JSX from app.js!',
    options: []
}

const appRoot = document.getElementById("app");

const renderFormApp = () => {
    const template = (
        <div>
            <h1>{appInfo.title}</h1> 
            {appInfo.subtitle && <p>{appInfo.subtitle}</p>}
        </div>
    );

    ReactDOM.render(template, appRoot)
}

// renderFormApp()
ReactDOM.render(<IndecisionApp />, appRoot)