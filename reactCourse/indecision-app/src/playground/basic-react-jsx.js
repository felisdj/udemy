console.log("app.js is running!");

// if statement
// ternary operators
// logical and operator

// only rener the subtitle (and p tag) if subtitle exist - logical and operator
// render new p tag - if options.length > 0 "Here are your options" , "No options"

// JSX - JavaScript XML

const appInfo = {
    title: 'Indecision App',
    subtitle: 'This is JSX from app.js!',
    options: []
}

// appInfo.options = []

const onFormSubmit = e => {
  e.preventDefault();
  console.log("form submitted!");
};
const onRemoveOptions = () => {
    appInfo.options = []
    renderFormApp()
}

// Render list array JSX
const updateOptions = () => {
    let optionsJSX = appInfo.options.map((option, index) => <li key={index}>{option}</li> )
    console.log(optionsJSX)
    return optionsJSX
}

const onMakeDecision = () => {
    const randomNum = Math.floor(Math.random() * appInfo.options.length)
    const option = appInfo.options[randomNum]
    if (option) {
        alert(option)
    }
}

const renderFormApp = () => {
    const template = (
        <div>
            <h1>{appInfo.title}</h1> 
            {appInfo.subtitle && <p>{appInfo.subtitle}</p>}
            {checkOptions(appInfo.options)}
            <button disabled={appInfo.options.length === 0} onClick= {onMakeDecision}>What should I do?</button>
            <button onClick={onRemoveOptions}>Remove All</button>
            <ol>
                {updateOptions()}
            </ol>

            <form onSubmit={onFormFunction}>
                <input type="text" name="optionField" />
                <button>Add Option</button>
            </form>
        </div>
    );

    ReactDOM.render(template, appRoot)
}

function onFormFunction(e) {
  e.preventDefault();
  
    const option = e.target.elements.optionField.value // --> e.(event target).(All Elements).(element's name).value

    if (option) {
        appInfo.options.push(option)
        e.target.elements.optionField.value = '';
        renderFormApp()
        console.log('option :', option)
    }
}

function checkOptions(options) {
    if(options) {
        if(options.length > 0) {
            return <p>Here are your options</p>
        } else {
            return <p>No options</p>
        }
    }
}
// Create a templeteTwo var JSX expression
// div
//  h1 -> Felis Domesticus
//  p  -> Age : 23
//  p  -> Location : Thailand
// Render templateTwo instead of template

// create app object title/subtitle
// use title/subtitle in the template
// render templete

const myInfo = {
  name: "Felis J",
  age: 23,
  location: "Thailand"
};

const template2 = CreateMyProfile(myInfo);

function CreateMyProfile(user) {
    return (
        <div>
            <h1>{user.name || 'Anonymous'}</h1>
            {getAge(user.age)}
            {/* <p>Location : {getLocation(user.location)}</p> */}
            {getLocation(user.location)}
        </div>
    )
}

function getAge(age) {
    // true && Variable  -> Bariable
    // false && Variable -> false (ignore on JSX)
    return age && <p>Age: {age}</p> // age >= 18 && <p>Age : {age}</p>
    // if (age) {
    //     return <p>Age : {age}</p>
    // }
}
function getLocation(location) {
    if (location) {
        return <p>Location : {location}</p>;
    }
    // return 'Unknown'
}

// Event & Attributes
let count = 0
const addOne = () => {
    count++
    renderCounterApp()
}
const minusOne = () => {
    count--
    renderCounterApp()
}
const reset = () => {
    count = 0
    renderCounterApp()
}

const appRoot = document.getElementById("app");

const renderCounterApp = () => {
    const template3 = (
        <div>
            <h1>Count : {count}</h1>
            <button onClick={addOne}>+1</button>
            <button onClick={minusOne}>-1</button>
            <button onClick={reset}>reset</button>
        </div>
    )
    
    // ReactDOM.render(template3, appRoot);
}

// renderCounterApp()

// ReactDOM.render(template, appRoot);
renderFormApp()