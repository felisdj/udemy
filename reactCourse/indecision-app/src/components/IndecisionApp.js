import React from "react"

import Header from "./Header"
import AddOption from "./AddOption"
import Options from "./Options"
import Action from "./Action"
import OptionModal from "./OptionModal"

export default class IndecisionApp extends React.Component {
    constructor(props) {
        super(props)
        this.handleDeleteOptions = this.handleDeleteOptions.bind(this)
        this.handlePick = this.handlePick.bind(this)
        this.handleRemovingIndividualOption = this.handleRemovingIndividualOption.bind(this)
        this.handleAddOption = this.handleAddOption.bind(this)
        this.handleClearSelectedOption = this.handleClearSelectedOption.bind(this)
        this.state = {
            options: props.options,
            selectedOption: undefined
        }
    }

    componentWillMount() {
        console.log("fetching data firsttime.")
    }
    componentDidMount() {
        try {
            const options = JSON.parse(localStorage.getItem("options"))
            if (options) {
                console.log(options)
                // this.state = json
                this.setState( () => ({options}) )
            }
            console.log("fetching data")
        } catch (e) {
            console.log("error :", e)
        }
    }
    componentDidUpdate(prevProps, prevState) {
        console.log( prevState.options, this.state.options)
        if (prevState.options.length !== this.state.options.length) {
            const json = JSON.stringify(this.state.options)
            localStorage.setItem('options', json)
            console.log('saving data')
        }
    }
    componentWillUnmount() {
        console.log("componentWillUnmount")
    }

    handleClearSelectedOption() {
        this.setState( () => ({ selectedOption : undefined }))
    }
    handleDeleteOptions() {
        // this.setState( () => {
        //     return {
        //         options: []
        //     }
        // })

        this.setState( () => ({
            options: []
        }))

        // this.setState( () => ({ options: [] }))
    }
    
    handleRemovingIndividualOption(option) {
        this.setState( (prevState => {
            const indexOfOption = prevState.options.indexOf(option)
            let options = prevState.options
            if(indexOfOption > -1) {
                // prevState.options.splice(indexOfOption, 1)
            }
            return { options : options.filter( (opt) => opt !== option ) }
        }))
    }

    handlePick() {
        let optionIndex = Math.floor(Math.random() * this.state.options.length)

        this.setState( () => ({ selectedOption : this.state.options[optionIndex] }) )
        // alert(this.state.options[optionIndex])
    }

    handleAddOption(option) {
        if (!option) {
            return "Pls enter value to add option."
        } else if (this.state.options.indexOf(option) > -1) {
            return "This option already exists."
        }
        this.setState( (prevState) => {
            // prevState.options.push(option)
            return {
                options: prevState.options.concat([option]) // --> cant push at here
            }
        })
    }

    render() {
        const subtitle = "Put your life in the hands of a computer"

        return (
            <div>
                <Header subtitle={subtitle}/>
                <div className="container">
                    <Action 
                        hasOptions={this.state.options.length > 0} 
                        handlePick={this.handlePick}
                    />
                    <div className="widget">
                        <Options 
                            options={this.state.options} 
                            handleDeleteOptions={this.handleDeleteOptions}
                            handleRemovingIndividualOption={this.handleRemovingIndividualOption} 
                        />
                        <AddOption 
                            handleAddOption={this.handleAddOption}
                        />
                    </div>
                    <OptionModal 
                        selectedOption={this.state.selectedOption}
                        handleClearSelectedOption={this.handleClearSelectedOption}    
                    />
                </div>
            </div>
        )
    }
}

IndecisionApp.defaultProps = {
    options: []
}