import React from "react"

export default class AddOption extends React.Component {
    constructor(props) {
        super(props)
        this.handleAddOption = this.handleAddOption.bind(this)
        this.state = {
            error: undefined
        }
    }

    handleAddOption(e) {
        e.preventDefault();
        // console.log(testting)
        const option = e.target.elements.optionField.value.trim() // --> e.(event target).(All Elements).(element's name).value
        const error = this.props.handleAddOption(option)
        this.setState( () => ({ error }))
    /*
        this.setState( () => {
            return {
                // error: error
                error // <-- the same name can use this
            }
        })
    */
        e.target.elements.optionField.value = '';
    }

    render() {
        return (
            <div>
                {this.state.error && <p className="add-option-error">{this.state.error}</p>}
                <form
                    className="add-option"
                    onSubmit={this.handleAddOption}
                >
                    <input type="text" name="optionField"></input>
                    <button className="button">Add option</button>
                </form>
            </div>
        )
    }
}
