import React from "react"

const Option = (props) => {
    return (
        <div className="option">
            <p className="option__text">{props.count}. {props.optionText}</p>
            <button 
                className="button button--link"
                onClick={() => {
                    props.handleRemovingIndividualOption(props.optionText)
                }}
            >
                remove
            </button>
        </div>
    )
}

// class Option extends React.Component {
//     render() {
//         return (
//             <div>
//                 Option : {this.props.optionText}
//             </div>
//         )
//     }
// }

export default Option
// export { Option as default }