import React from "react"
import Option from "./Option"
// class Remove extends React.Component {
//     handleRemoveAll() {
//         alert("remove all option")
//     }

//     render() {
//         return (
//             <div>
//                 <button onClick={this.handleRemoveAll}>Reset all</button>
//             </div>
//         )
//     }
// }
const Remove = (props) => {
    return (
        <button 
            className="button button--link"
            onClick={props.handleDeleteOptions}
            disabled={props.disabled}
        >
            Remove all
        </button>
    )
}

const Options = (props) => {
    return (
        <div>
            <div className="widget-header">
                <h3>You Options</h3>
                <Remove 
                    handleDeleteOptions={props.handleDeleteOptions} 
                    disabled={props.options.length === 0}
                />
            </div>
            {props.options.length === 0 && <p className="widget-message">Plase add some option.</p>}
            {
                props.options.map( (option, index) => 
                    <Option 
                        key={index}
                        count={index+1}
                        optionText={option} 
                        handleRemovingIndividualOption={props.handleRemovingIndividualOption}
                    /> 
                )
            }
        </div>
    )
}
// class Options extends React.Component {
//     constructor(props) {
//         super(props)
//     }

//     render() {
//         return (
//             <div>
//                 {/* <Remove /> */}
//                 <button onClick={this.props.handleDeleteOptions}>
//                     Remove all
//                 </button>
//                 {
//                     this.props.options.map( (option, index) => <Option key={index} optionText={option} /> )
//                 }
//             </div>
//         )
//     }
// }

export default Options