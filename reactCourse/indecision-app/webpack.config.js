// entry -> output
const path = require("path")

// console.log(path.join(__dirname, "public"))

module.exports = {
    entry: "./src/app.js",
    // mode: "development",
    // mode: "production",
    mode: "none",
    output: {
        path: path.join(__dirname, "public"),
        filename: "bundle.js"
    },
    module: {
        rules: [{
            loader: "babel-loader",
            test: /\.js$/,
            exclude: /node_modules/
        }, {
            test: /\.s?css$/,
            use: [
                "style-loader",
                "css-loader",
                "sass-loader"
            ] // array of loader
        }]
    },
    devtool: "cheap-module-eval-source-map", // <-- for dubug
    devServer: {
        contentBase: path.join(__dirname, "public")

    }
}

// loader --> when webpack found some jsx on js file, it does something
