import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';

const fetch = require("node-fetch")

async function fetchTodo(url) {
    try {
        const result = await fetch(url)
        const todo = result.json()

        return todo
    } catch (e) {
        console.log(e.message)
    }
}

fetchTodo("https://gateway.sobtid.me/sobtid_jr/v1/terms-of-service")
    .then(todo => console.log(todo))

fetch('http://restcountries.eu/rest/v2/all').then((response) => {
    if (response.status === 200) {
        return response.json()
    } else {
        throw new Error("error")
    }
}).then((data) => {
    console.log(data[0])
}).catch((err) => {
    console.log(err)
})

const App = (props) => {
    // const [count, setCount] = useState(props.count)

    // const increment = () => {
    //     setCount(count + 1)
    // }

    const [state, setState] = useState({
        count: props.count,
        text: props.text
    })

    return (
        <div>
            <p>{state.text || "count"} is {state.count}</p>
            <button onClick={() => setState({ ...state, count: state.count + 1 })}>+1</button>
            <button onClick={() => setState({ ...state, count: state.count - 1})}>-1</button>
            <button onClick={() => setState({ ...state, count: props.count})}>reset</button>
            <input type="text" value={state.text} onChange={(e) => {
                setState({ ...state, text: e.target.value})
            }} />
        </div>
    )
}

App.defaultProps = {
    count: 0,
    text: ''
}

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();