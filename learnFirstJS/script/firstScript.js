var x = 10
var y = 3

Math.pow(x, y)

let fruits = ["Apple", "Mango", "Orange", "Strawberry"]

// alert(fruits[5] + ' ' + fruits[0])

// remove first index
var firstFruit = fruits.shift()
alert(firstFruit)

// remove last index
var lastFruit = fruits.pop() 
alert(lastFruit)

// insert first
fruits.unshift("Banana")

// insert last
fruits.push("Blueberry")

console.log('Now last : ' + fruits[fruits.length-1])

// LOOP
for (let i = 0; i < fruits.length; i++) {
    const fruit = fruits[i];
    console.log("index " + i + " is " + fruit)
}

fruits.forEach(fruit => {
    console.log(fruit);

});


switch (firstFruit) {
    case "Apple":
        alert("Apple")
        break;
    case "Mango":
        alert("Mango")
        break;
    case "Banana":
        alert("Banana")
        break
    default:
        break;
}

// Get value of an element in JS
let elm = document.getElementById("firstDiv").innerHTML

let fieldVal = document.getElementById("myField").value

function clickMe() {
    alert("CLick On Button.")
}

function keyDown(event) {
    var key = event.keyCode || event.which
    if(key === 13) {
        return
    }
    console.log("test key down. $(key)")
}

function validateForm() {
    firstName = document.getElementById("firstName").value
    lastName = document.getElementById("lastName").value

    document.getElementById("resultPart").innerHTML = 'Hello ' + firstName + ' ' + lastName
}

function limitText() {
    textElement = document.getElementById("firstName")
    text = textElement.value
    countElement = document.getElementById("limitCount")

    if(text.length < 11) {
        countElement.innerHTML = 10 - text.length
    } else {
        textElement.value = text.substring(0, 10)
    }
}

// MARK : Music
let isPlaying = false
const musicPlayer = new Audio()
musicPlayer.src = "music.m4a"

function playMusic() {
    let playButton = document.getElementById("playMusic")
    if(!this.musicPlayer) {
        this.musicPlayer = new Audio()
        this.musicPlayer.src = "music.m4a"
    }
    if(this.isPlaying) {
        this.musicPlayer.pause()
        playButton.value = "Play"
    } else {
        this.musicPlayer.play()
        playButton.value = "Stop"
        timeCounting()
    }
    this.isPlaying = !this.isPlaying
}

function resetMusic() {
    this.musicPlayer.currentTime = 0
    this.timeCount = 0
    document.getElementById("timeCountText").innerHTML = this.timeCount;
}

// MARK : Event Close
window.onbeforeunload = function () {
    return "test before close tab."
}

// MARK : Timer
function timeCounting() {
    console.log("time counting")
    if(!this.timeCount) {
        this.timeCount = 0
        console.log("set default time")
    } else {
        console.log("timeCount is existed : " + this.timeCount)
    }
    if(this.timeCount == 0) {
        console.log("set interval")
        setInterval(() => {
            let nextTime = parseInt(this.timeCount) + 1
            console.log("this time " + nextTime)
            this.timeCount = nextTime
            document.getElementById("timeCountText").innerHTML = this.timeCount
        }, 1000);
        // MARK : Countdown
        setTimeout(() => {
            document.getElementById("resetMusic").value = "RESET"
        }, 2000);
    }
    
}